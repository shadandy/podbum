import ldap3
import flask_ldap3_login
from flask_ldap3_login import LDAP3LoginManager
import logging
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import InputRequired
#from my_app import db, app, session, cipher_suite
from my_app import db, app, cipher_suite
from my_app.auth import database
from sqlalchemy import UniqueConstraint, Index
from sqlalchemy.exc import DatabaseError

class Session(db.Model):
    __tablename__ = 'sessions'
    id = db.Column(db.Integer, primary_key=True, index=True)
    session_id = db.Column(db.String(255), index=True, unique=True)
    data = db.Column(db.LargeBinary)
    expiry = db.Column(db.Time, index=True)

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True, index=True)
    username = db.Column(db.String(100), nullable=False, unique=True, index=True)
    databases = db.relationship('DatabaseUser', lazy=False)

    def __init__(self, username):
        self.username = username.upper()

    def __repr__(self):
        return self.username

    @staticmethod
    def try_login(domain, username, password):
        # Setup a LDAP3 Login Manager.
        ldap_manager = LDAP3LoginManager()

        # Init the manager with the config since we aren't using an app
        ldap_config = app.config['LDAP_DOMAINS'].get(domain)
        ldap_config['LDAP_BIND_USER_DN'] = ldap_config['LDAP_USER_LOGIN_ATTR']+'='+username+','+ldap_config['LDAP_USER_DN']+','+ldap_config['LDAP_BASE_DN']
        ldap_config['LDAP_BIND_USER_PASSWORD'] = password

        ldap_manager.init_config(ldap_config)

        # Check if the credentials are correct
        response = ldap_manager.authenticate(username, password)

        return response

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

class DatabaseUser(db.Model):
    __tablename__ = 'database_user'
    __table_args__ = (
        UniqueConstraint("database_name", "user_id"),
    )
    id = db.Column(db.Integer, primary_key=True, index=True)
    database_name = db.Column(db.String(100), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    account_status = db.Column(db.String(100), nullable=False)

    def __init__(self, database_name, user_id, account_status):
        self.database_name = database_name
        self.user_id = user_id
        self.account_status = account_status

    def get_id(self):
        return unicode(self.id)

    def get_connection_string(self):
        conn_string = app.config['DATABASE_CONNECTIONS'].get(self.database_name)
        return conn_string

    def set_account_status(self, account_status):
        if account_status != None:
            self.account_status = account_status
        else:
            self.account_status = "UNKNOWN"
        db.session.commit()

    def get_account_status(self):
        return unicode(self.account_status)

    def reset_password(self, encrypted_password):
        conn_string = self.get_connection_string()
        db_user = User.query.get(self.user_id)
        password = cipher_suite.decrypt(encrypted_password).decode()
        try:
            oradb_session = database.get_db_session(conn_string)
            stmt = 'ALTER USER "'+db_user.username.upper()+'" IDENTIFIED BY "'+password+'" ACCOUNT UNLOCK'
            result = oradb_session.execute(stmt)
            result = oradb_session.execute('SELECT account_status FROM dba_users WHERE username = :username', {'username': db_user.username.upper()}).first()
            self.set_account_status(result.account_status)
            oradb_session.close()
        except DatabaseError as e:
            msg = e.args[0] 
            raise DatabaseError(stmt.replace(password,'XXXXXXXX'), None, msg.replace(password,'XXXXXXXX'), hide_parameters=True, connection_invalidated=False, code=None) from None

    def unlock_account(self):
        conn_string = self.get_connection_string()
        db_user = User.query.get(self.user_id)
        oradb_session = database.get_db_session(conn_string)
        result = oradb_session.execute('ALTER USER "'+db_user.username.upper()+'" ACCOUNT UNLOCK')
        result = oradb_session.execute('SELECT account_status FROM dba_users WHERE username = :username', {'username': db_user.username.upper()}).first()
        self.set_account_status(result.account_status)
        oradb_session.close()

class LoginForm(FlaskForm):
    username = StringField('Username', [InputRequired()])
    password = PasswordField('Password', [InputRequired()])
