from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from my_app import app

def get_db_session(conn_string):
    engine = create_engine(conn_string, echo=app.config["SQLALCHEMY_TRACK_MODIFICATIONS"], poolclass=NullPool)

    OraBase = declarative_base()

    Session = sessionmaker(bind=engine)
    session = Session()

    return session
