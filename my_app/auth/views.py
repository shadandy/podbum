import logging
import ldap3
from flask import session, request, render_template, flash, redirect, \
    url_for, Blueprint, g, Markup
from flask_session import Session
from flask_ldap3_login import AuthenticationResponseStatus
from flask_login import current_user, login_user, \
    logout_user, login_required
from my_app import login_manager, db, app, cipher_suite
from my_app.auth.models import User, DatabaseUser, LoginForm
from my_app.auth import database
from sqlalchemy import *
from sqlalchemy.exc import DatabaseError
from cryptography.exceptions import InvalidSignature
from cryptography.fernet import InvalidToken

logging.basicConfig(filename=app.config["LOG_FILE"],level=logging.DEBUG, format=f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

auth = Blueprint('auth', __name__)

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))

@login_manager.unauthorized_handler
def unauth_handler():
    flash("Your session timed out. Please login again.","warning")
    return redirect(url_for('auth.login'))

@auth.before_request
def get_current_user():
    g.user = current_user
    session.modified = True

@auth.route('/')
@auth.route('/home')
def home():
    if current_user.is_authenticated:
        user_databases = DatabaseUser.query.join(User).filter(User.username==current_user.username.upper()).order_by(DatabaseUser.database_name).all()
        return render_template('home.html', username=current_user.username, user_databases=user_databases)

    return redirect(url_for('auth.login'))

@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        flash('You are already logged in.')
        return redirect(url_for('auth.home'))

    form = LoginForm()

    if request.method == 'POST' and form.validate():
        domain = request.form.get('domain')
        username = request.form.get('username')
        password = request.form.get('password')

        login_response = User.try_login(domain, username, password)
        if login_response.status == AuthenticationResponseStatus.fail:
            flash('Invalid username or password. Please try again.','danger')
            logging.warning("Failed Logon: "+username.upper())
            return render_template('login.html', form=form, domains=app.config["LDAP_DOMAINS"])

        user = User.query.filter_by(username=username.upper()).first()

        if not user:
            user = User(username)
            db.session.add(user)
            db.session.commit()
        login_user(user)

        logging.info("Logon: "+username.upper())

        session['password'] = cipher_suite.encrypt(str.encode(password)) 

        flash('You have successfully logged in.', 'success')

        return redirect(url_for('auth.home'))

    for error in form.errors:
        if error == 'csrf_token':
            flash("Your session cookie expired. Please login again.","warning")
        else: 
            flash("Error: "+error, 'danger')

    return render_template('login.html', form=form, domains=app.config["LDAP_DOMAINS"])

@auth.route('/reset', methods=['POST'])
@login_required
def reset():

    dbuserid = request.form.get('dbuserid')
    dbuser = DatabaseUser.query.get(int(dbuserid))

    if current_user.id != dbuser.user_id:
        flash("You cannot change another user's password.","warning")
        return redirect(url_for('auth.home'))

    try:
        dbuser.reset_password(session['password'])
    except DatabaseError as e:
        err_msg="Error Message: %s" % e
        #err_msg=err_msg.replace(session['password'],'XXXXXXXX')
        flash("An error was encountered while resetting the password for "+current_user.username.upper()+" account on "+dbuser.database_name+".", "danger")
        # Handle people trying to synchronize the password if the database policy does not allow reuse.
        if "ORA-28007" in err_msg:
            flash(Markup("This tool attempts to synchronize your network password to your database account.<br>" +
                         "You have either used the same password in the past, or your database password is currently set to your network password.<br>" +
                         "  <b>Option 1:</b> \"Unlock\" the account rather than resetting the password, and verify whether you are able to login.<br>" +
                         "  <b>Option 2:</b> Change your network password to something you have not used in the past and try again."), "warning")
        elif "ORA-12541" in err_msg:
            flash("The database listener is not running. Try again later, or contact IT.", "warning")
        elif "ORA-12514" in err_msg:
            flash("The database listener is not able to connect to the database. Try again later, or contact IT.", "warning")
        elif "ORA-01109" in err_msg:
            flash("The database is running but is not open. Try again later, or contact IT.", "warning") 
        elif "ORA-28547" in err_msg:
            flash("The database is offline. Try again later, or contact IT.", "warning")
        else:
            flash(err_msg,"warning")
        logging.exception("Database: "+dbuser.database_name+", "+err_msg)
        return redirect(url_for('auth.home'))
    except InvalidSignature as e:
        flash("The application was restarted. Please login again.", "warning")
        return redirect(url_for('auth.logout'))
    except InvalidToken as e:
        flash("The application was restarted. Please login again.", "warning")
        return redirect(url_for('auth.logout'))
 
    logging.info("Reset: "+current_user.username.upper()+"@"+dbuser.database_name)
    flash("The "+dbuser.database_name+" database password for "+current_user.username.upper()+" has been reset to your network password.", "success")
    return redirect(url_for('auth.home'))

@auth.route('/unlock', methods=['POST'])
@login_required
def unlock():

    dbuserid = request.form.get('dbuserid')
    dbuser = DatabaseUser.query.get(int(dbuserid))

    if current_user.id != dbuser.user_id:
        flash("You cannot change another user's password.","warning")
        return redirect(url_for('auth.home'))

    try:
        dbuser.unlock_account()
    except DatabaseError as e:
        flash("An error was encountered while unlocking the "+current_user.username.upper()+" account on "+dbuser.database_name+".", "danger")
        flash("Error Message: %s" % e,"warning")
        logging.exception("Database: "+dbuser.database_name+", Error: %s" % e)
        return redirect(url_for('auth.home'))

    logging.info("Unlock: "+current_user.username.upper()+"@"+dbuser.database_name)
    flash("The "+current_user.username.upper()+" database account on "+dbuser.database_name+" has been unlocked.", "success")
    return redirect(url_for('auth.home'))


@auth.route('/logout')
@login_required
def logout():
    logging.info("Logout: "+current_user.username.upper())
    logout_user()
    return redirect(url_for('auth.login'))
