from flask import Flask, session
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from cryptography.fernet import Fernet

app = Flask(__name__)
app.config.from_pyfile('app.config')
app.config['ENCRYPTION_KEY'] = Fernet.generate_key()
app.config['SESSION_TYPE'] = 'sqlalchemy'
app.config['SESSION_SQLALCHEMY_TABLE'] = 'sessions'
cipher_suite = Fernet(app.config['ENCRYPTION_KEY'])
Session(app)
app.session_interface.db.create_all()
db = SQLAlchemy(app)

SESSION_SQLALCHEMY=db
 
login_manager = LoginManager(app)
login_manager.login_view = 'auth.login'

from my_app.auth.views import auth
app.register_blueprint(auth)

db.create_all()
db.session.remove()
db.engine.dispose()
