#!/usr/bin/env bash

export LD_LIBRARY_PATH=/app/podbum/instantclient
export TNS_ADMIN=/app/podbum/podbum/wallet

cd $HOME/podbum && python3 dbload.py
