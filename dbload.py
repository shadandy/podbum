import logging
from datetime import datetime
from my_app import db, app, session
from my_app.auth import database
from my_app.auth.models import User, DatabaseUser
from cx_Oracle import DatabaseError

# Delete database users for any connections that were removed from the configuration file.
for row in db.session.query(DatabaseUser.database_name).distinct().all():
    dbexists = False
    for db_name in app.config['DATABASE_CONNECTIONS'].keys():
        if db_name == row.database_name:
            dbexists = True
    if not dbexists:
        db.session.query(DatabaseUser).filter(DatabaseUser.database_name==row.database_name).delete()
        db.session.commit()
        logging.info("Database removed from configuration. Deleted all podbum database users for "+row.database_name+".")

for db_name, conn_string in app.config['DATABASE_CONNECTIONS'].items():

    logging.info("Processing "+db_name+" database user inventory.")

    try:
        # Get list of actual database users for this database.
        oradb_session = database.get_db_session(conn_string)
        results = oradb_session.execute("SELECT username, account_status FROM dba_users WHERE oracle_maintained <> 'Y'")
        resultset = [dict(row) for row in results]
        oradb_session.close()

    except Exception as e:
        # Delete the users from the podbum database if the connection fails. We'll assume the users will not be able to connect to the database either.
        db.session.query(DatabaseUser).filter(DatabaseUser.database_name==db_name).delete()
        db.session.commit()
        logging.info("Error retrieving list of database users. Deleted all podbum database users for "+db_name+".")
        logging.info("Database: "+db_name+", Error: %s" % e)
        continue

    try:
        # Get list of last known database users for this database.
        for existing_dbuser in db.session.query(DatabaseUser.id, DatabaseUser.database_name, User.username).join(User).filter(DatabaseUser.database_name==db_name).all():
            thisuser = ''
            thisuserstatus = ''
            userexists = False
            for result in resultset:
                thisuser = result['username']
                thisuserstatus = result['account_status']
                if existing_dbuser.username == thisuser:
                    userexists = True
            if not userexists:
                DatabaseUser.query.filter_by(id=existing_dbuser.id).delete()
                logging.info("Deleted non-existing user "+existing_dbuser.username+"@"+db_name+".")
 
        for result in resultset:
                 
            thisuser = result['username']
            thisuserstatus = result['account_status']
            user = User.query.filter(User.username==thisuser).first()
            # Skip existing users.
            if bool(user):
                pass
            # Add user if not existing.
            else:
                user = User(thisuser)
                db.session.add(user)
                logging.info("Added new user: "+user.username)

            dbuser = DatabaseUser.query.join(User).filter(User.username==thisuser).filter(DatabaseUser.database_name==db_name).first()
            # Update database user account status if existing.
            if bool(dbuser):
                if dbuser.account_status != thisuserstatus:
                    dbuser.account_status = thisuserstatus
                    logging.info("Updated user "+thisuser+"@"+db_name+" account status to "+thisuserstatus+".")
            # Add database user if not existing.
            else:
                dbuser = DatabaseUser(db_name, user.id, thisuserstatus)
                db.session.add(dbuser)
                logging.info("Added database user "+user.username+"@"+db_name+".")


    except DatabaseError as e:
            logging.exception("Database: "+db_name+", Error: %s" % e)

    db.session.commit()

    # Delete expired sessions to keep the database clean.
    session_interface=app.session_interface
    expired_sessions=session_interface.sql_session_model.query.filter(session_interface.sql_session_model.expiry <= datetime.utcnow())
    for es in expired_sessions:
        session_interface.db.session.delete(es)
    session_interface.db.session.commit()

