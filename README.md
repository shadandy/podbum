# Personal Oracle Database User Manager

## Table of Contents
[TOC]

## Overview

Are you an Oracle database administrator? I am, and I got sick and tired of resetting user passwords. LDAP authentication would resolve most of the pain points, but Oracle does not make LDAP authentication exactly easy or cheap. Then you throw in Amazon RDS or another cloud provider, and you lose host access to actually make Oracle authenticate against anything but itself. Awesome, thanks, Oracle...

This is where Personal Oracle Database User Manager, aka PODBUM, comes into play.

Personal Oracle Database User Manager is a lightweight Python Flask application that allows users to unlock their Oracle database accounts and reset their passwords. It does this by populating a list of database users for each configured Oracle Database connection via a scheduled job. A user logs into the application by authenticating against the central LDAP directory. The user sees a list of his/her database accounts as well as "Reset Password" and "Unlock Account" buttons. Clicking the "Reset Password" button will synchronize the user's database password with their LDAP password. Clicking the "Unlock Account" button will simply unlock the user's Oracle account without changing the password.

The app makes the following assumptions:

- The user's LDAP directory username matches their Oracle Database username.
- Oracle Database accounts are dropped when a user loses access to them.
- The Oracle Database password will be reset to the user's LDAP password.

Personal Oracle Database User Manager is free to use. That's all there is to it.

## Screenshots

![Login Page Screenshot](/my_app/static/images/login01.png "Login Page")

![Home Page Screenshot](/my_app/static/images/home01.png "Home Page")

![Password Reset Screenshot](/my_app/static/images/reset01.png "Password Reset")

![Account Unlock Screenshot](/my_app/static/images/unlock01.png "Account Unlock")

## Installation Requirements

- PODBUM requires a small Linux server and has been tested on RHEL-based and Debian-based systems.
- An SSL certificate, because you do not want passwords going over the wire in clear text.
- [Oracle Instant Client](https://www.oracle.com/technetwork/database/database-technologies/instant-client/downloads/index.html)
- [Miniconda3](https://docs.conda.io/en/latest/miniconda.html)

## Installation Instructions

Create an OS user that will own the PODBUM install. For the purposes of this document, the username will be "podbum".

Install OS prerequisites. Instructions are for a RHEL/Centos 7 based system minimal install. If so inclined, you can work through getting SELinux and the firewall working after installing everything. Both have been disabled in the instructions below.
```
sudo yum -y install epel-release
sudo yum -y install openssh-server wget unzip zip bzip2 nginx
sudo systemctl start sshd
sudo systemctl enable sshd
sudo systemctl stop firewalld.service
sudo systemctl disable firewalld.service
sudo setenforce 0
```

Download the Oracle Instant Client for Linux x86-64. The "Basic Package" is sufficient.

Install the Oracle Instant Client. (Be sure to replace instantclient_19_3 with the version you are using.)
```
sudo su - podbum
mkdir ~/oracle
cd ~/oracle
unzip ~/instantclient-basic-linux.x64-19.3.0.0.0dbru.zip
echo "TCP.CONNECT_TIMEOUT=5" > ~/oracle/instantclient_19_3/network/admin/sqlnet.ora
echo "export LD_LIBRARY_PATH=$HOME/oracle/instantclient_19_3:\$LD_LIBRARY_PATH" >> ~/.bashrc
```

Install Miniconda3. You can accept the default prompts.
```
cd
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
sh Miniconda3-latest-Linux-x86_64.sh
```

Install Python modules.
```
. ~/.bash_profile
conda config --add channels conda-forge
conda install cx_oracle flask flask-login flask-ldap3-login flask-sqlalchemy flask-wtf pip uwsgi
pip install flask-session
```

Install the Personal Oracle Database User Manager application code.
```
cd
wget https://bitbucket.org/shadandy/podbum/get/master.zip
unzip master.zip
ln -s shadandy-podbum-* podbum
```

## Configuration Instructions

### Oracle Database Configuration
PODBUM will need to connect to each database as a user with CREATE SESSION, ALTER USER, and SELECT ON DBA_USERS privileges. I recommend creating a dedicate application user for this purpose, although you could use a privileged account like SYSTEM. For example:

```
CREATE USER PODBUM_APP IDENTIFIED BY "&YOUR_PASSWORD";
GRANT CREATE SESSION TO PODBUM_APP;
GRANT ALTER USER TO PODBUM_APP;
GRANT SELECT ON DBA_USERS TO PODBUM_APP;
```

### Personal Oracle Database User Manager Configuration
PODBUM configurations reside in the ~/podbum/my_app/app.config file, and most configurations will be related to your specific environment. The example configuration settings will give you a reasonable idea of what needs to be updated.

#### Configuration File

Edit the example configuration file to fit your environment.

```
vi ~/podbum/my_app/app.config
```

Add DATABASE_CONNECTIONS for each database you have. The first key value, such as 'ORCLPDB1', is an arbitrary connection name. Using the database name is recommended. The second value is the actual connection string.

```
SQLALCHEMY_DATABASE_URI='sqlite:///podbum.db'
SQLALCHEMY_TRACK_MODIFICATIONS=False
DATABASE_CONNECTIONS = {
    'ORCLPDB1': 'oracle://podbum_app:password@dbhost1.yourdomain.com:1521/?service_name=ORCLPDB1',
    'ORCLPDB2': 'oracle://podbum_app:password@dbhost2.yourdomain.com:1521/?service_name=ORCLPDB2',
    'ORCLPDB3': 'oracle://podbum_app:password@dbhost3.yourdomain.com:1521/?service_name=ORCLPDB3'
}
LDAP_DOMAINS = {
    'DOMAIN1':{'LDAP_HOST':'ldap.domain1.com','LDAP_PORT':636,'LDAP_USE_SSL':True,'LDAP_BIND_DIRECT_CREDENTIALS':True,'LDAP_BASE_DN':'DC=domain1,DC=com','LDAP_USER_DN':'CN=users'},
    'DOMAIN2':{'LDAP_HOST':'ldap.domain2.com','LDAP_PORT':636,'LDAP_USE_SSL':True,'LDAP_BIND_DIRECT_CREDENTIALS':True,'LDAP_BASE_DN':'DC=domain2,DC=com','LDAP_USER_DN':'CN=users'}
}
SECRET_KEY='put your secret key here'
LOG_FILE='podbum.log'
PERMANENT_SESSION_LIFETIME=900
```

The LDAP settings may take some trial and error to sort out. The configuration file allows multiple domains to be added, although most users should just need one.

#### Configuration Testing

To test the configuration up to this point, start the development server.
```
cd ~/podbum
python run.py
```

Try to log into the application using your LDAP credentials. 

http://yourhost.yourdomain.com:5000

If the application is not up or if your login credentials do not work, review the ~/podbum/podbum.log file for relevant errors. Correct, and try again...

#### Load Database Users

After verifying that application logins work as expected, populate the database user list by running the following script. The script loads tables in a local SQLite database.

```
cd ~/podbum
./dbload.sh
```

Log into the application, and you should now see a list of all your database accounts. Test unlocking an account and/or resetting a password.

#### Schedule Database User Load Job

The database users are periodically loaded into the SQLite database via a scheduled job. The job connects to each configured database and runs ```SELECT username, account_status FROM dba_users WHERE oracle_maintained <> 'Y';```. This job also deletes expired application user sessions from the database. 

To schedule the job:
```
crontab -e

# Add the following to the crontab to synchronize the database user list every 10 minutes.
*/10 * * * * . ~/.profile; ~/podbum/dbload.sh >> ~/podbum/crontab.log 2>&1
```


### NGINX Configuration

Create an SSL certificate. Refer to your certificate authority's instructions for creating an official certificate. The instructions below are for a self-signed certificate.

```
sudo mkdir /etc/ssl/private
sudo chmod 700 /etc/ssl/private
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/podbum.key -out /etc/ssl/certs/podbum.crt
sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
```

Create an NGINX SSL configuration for PODBUM.

```
sudo echo '
server {
    listen 443 http2 ssl;
    listen [::]:443 http2 ssl;

    server_name server_IP_address;

    ssl_certificate /etc/ssl/certs/podbum.crt;
    ssl_certificate_key /etc/ssl/private/podbum.key;
    ssl_dhparam /etc/ssl/certs/dhparam.pem;

    ########################################################################
    # from https://cipherli.st/                                            #
    # and https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html #
    ########################################################################

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
    ssl_ecdh_curve secp384r1;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off;
    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8 8.8.4.4 valid=300s;
    resolver_timeout 5s;
    # Disable preloading HSTS for now.  You can use the commented out header line that includes
    # the "preload" directive if you understand the implications.
    #add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
    add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";
    add_header X-Frame-Options DENY;
    add_header X-Content-Type-Options nosniff;

    ##################################
    # END https://cipherli.st/ BLOCK #
    ##################################

    root /usr/share/nginx/html;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:/var/run/uwsgi/podbum.sock;
    }

    error_page 404 /404.html;
    location = /404.html {
    }

    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
    }

}' > /etc/nginx/conf.d/podbum-ssl.conf
```

Create an HTTP to HTTPS redirect.

```
sudo echo 'return 301 https://$host$request_uri/;' > /etc/nginx/default.d/ssl-redirect.conf
```

Test the NGINX configuration.

```
sudo nginx -t
```

If the configuration test does not report any errors, start NGINX.

```
sudo systemctl enable nginx
sudo systemctl start nginx
```

### uWSGI Configuration

Add the PODBUM OS user to the "nginx" group, and create required directories with correct permissions. Replace the "podbum" OS user as appropriate.

```
sudo usermod -G nginx podbum
sudo mkdir /var/run/uwsgi
sudo chmod 770 /var/run/uwsgi
sudo chown root:nginx /var/run/uwsgi
```

Configure the server to automatically create the /var/run/uwsgi directory on reboot.

```
sudo cp uwsgi.conf /usr/lib/tmpfiles.d/uwsgi.conf
```

Update the uwsgi.ini file to reflect the OS user that owns the PODBUM install.

```
vi ~/podbum/uwsgi.ini
```

Edit the following line, replacing "podbum" as appropriate:

```
chown-socket = podbum:nginx
```

Deploy the uWSGI PODBUM application service.

```
sudo cp podbum.service /etc/systemd/system/podbum.service
sudo systemctl enable podbum.service
sudo systemctl start podbum.service
```

Test that you can log into the PODBUM application and that SSL redirects work as expected.

## Celebrate!!!

You should now be setup. The next time a user requests a password change or account unlock, send them to your PODBUM URL!

If you find Personal Oracle Database User Manager helpful, please let us know how it is working for you.

And if you ever find yourself in need of Oracle DBA support or staff augmentation, please do visit our website at https://www.shadandy.com or reach out to us at info@shadandy.com.
